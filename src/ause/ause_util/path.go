package ause_util

import (
	"free5gc/lib/path_util"
)

var AuseLogPath = path_util.Gofree5gcPath("free5gc/ausesslkey.log")
var AusePemPath = path_util.Gofree5gcPath("free5gc/support/TLS/ause.pem")
var AuseKeyPath = path_util.Gofree5gcPath("free5gc/support/TLS/ause.key")
var DefaultAuseConfigPath = path_util.Gofree5gcPath("free5gc/config/ausecfg.conf")
