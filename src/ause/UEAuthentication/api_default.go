/*
 * AUSE API
 *
 * OpenAPI specification for AUSE
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package UEAuthentication

import (
	"github.com/gin-gonic/gin"
	"free5gc/lib/http_wrapper"
	"free5gc/lib/openapi/models"
	"free5gc/src/ause/ause_handler"
	"free5gc/src/ause/ause_handler/ause_message"
	"free5gc/src/ause/logger"
	// "fmt"
)

// EapAuthMethod -
func EapAuthMethod(c *gin.Context) {
	var eapSessionReq models.EapSession

	err := c.ShouldBindJSON(&eapSessionReq)
	if err != nil {
		logger.Auth5gAkaComfirmLog.Errorln(err)
	}

	req := http_wrapper.NewRequest(c.Request, eapSessionReq)
	req.Params["authCtxId"] = c.Param("authCtxId")

	handlerMsg := ause_message.NewHandlerMessage(ause_message.EventEapAuthComfirm, req)
	ause_handler.SendMessage(handlerMsg)
	rsp := <-handlerMsg.ResponseChan

	HTTPResponse := rsp.HTTPResponse
	c.JSON(HTTPResponse.Status, HTTPResponse.Body)
}

// UeAuthenticationsAuthCtxId5gAkaConfirmationPut -
func UeAuthenticationsAuthCtxId5gAkaConfirmationPut(c *gin.Context) {
	var confirmationData models.ConfirmationData

	err := c.ShouldBindJSON(&confirmationData)
	if err != nil {
		logger.Auth5gAkaComfirmLog.Errorln(err)
	}

	req := http_wrapper.NewRequest(c.Request, confirmationData)
	req.Params["authCtxId"] = c.Param("authCtxId")

	handlerMsg := ause_message.NewHandlerMessage(ause_message.EventAuth5gAkaComfirm, req)
	ause_handler.SendMessage(handlerMsg)
	rsp := <-handlerMsg.ResponseChan

	HTTPResponse := rsp.HTTPResponse
	c.JSON(HTTPResponse.Status, HTTPResponse.Body)
}

// UeAuthenticationsPost -
func UeAuthenticationsPost(c *gin.Context) {
	var authInfo models.AuthenticationInfo

	err := c.ShouldBindJSON(&authInfo)
	if err != nil {
		logger.UeAuthPostLog.Errorln(err)
	}

	req := http_wrapper.NewRequest(c.Request, authInfo)

	handlerMsg := ause_message.NewHandlerMessage(ause_message.EventUeAuthPost, req)
	ause_handler.SendMessage(handlerMsg)
	rsp := <-handlerMsg.ResponseChan

	HTTPResponse := rsp.HTTPResponse
	HTTPRespHeader := rsp.HTTPResponse.Header
	for k, v := range HTTPRespHeader {
		c.Header(k, v[0])
	}
	c.JSON(HTTPResponse.Status, HTTPResponse.Body)
}
