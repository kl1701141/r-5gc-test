package ause_handler

import (
	// "fmt"
	"github.com/sirupsen/logrus"
	"free5gc/lib/openapi/models"
	"free5gc/src/ause/ause_handler/ause_message"
	"free5gc/src/ause/ause_producer"
	"free5gc/src/ause/logger"
	"time"
)

const (
	MaxChannel int = 20
)

var auseChannel chan ause_message.HandlerMessage
var HandlerLog *logrus.Entry

func init() {
	HandlerLog = logger.HandlerLog
	auseChannel = make(chan ause_message.HandlerMessage, MaxChannel)
}

func SendMessage(msg ause_message.HandlerMessage) {
	auseChannel <- msg
}

func Handle() {
	for {
		select {
		case msg, ok := <-auseChannel:
			if ok {
				switch msg.Event {
				case ause_message.EventUeAuthPost:
					ause_producer.HandleUeAuthPostRequest(msg.ResponseChan, msg.HTTPRequest.Body.(models.AuthenticationInfo))
				case ause_message.EventAuth5gAkaComfirm:
					authCtxId := msg.HTTPRequest.Params["authCtxId"]
					ause_producer.HandleAuth5gAkaComfirmRequest(msg.ResponseChan, authCtxId, msg.HTTPRequest.Body.(models.ConfirmationData))
				case ause_message.EventEapAuthComfirm:
					authCtxId := msg.HTTPRequest.Params["authCtxId"]
					ause_producer.HandleEapAuthComfirmRequest(msg.ResponseChan, authCtxId, msg.HTTPRequest.Body.(models.EapSession))
				default:
					HandlerLog.Warnf("AUSE Event[%d] has not implemented", msg.Event)
				}
			} else {
				HandlerLog.Errorln("AUSE Channel closed!")
			}

		case <-time.After(time.Second * 1):

		}
	}
}
