package ause_producer

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"github.com/bronze1man/radius"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	// Nudm_UEAU "free5gc/lib/Nudm_UEAuthentication"
	"free5gc/lib/UeauCommon"
	"free5gc/lib/openapi/models"
	"free5gc/src/ause/ause_context"
	"free5gc/src/ause/ause_handler/ause_message"
	"free5gc/src/ause/logger"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

func HandleEapAuthComfirmRequest(respChan chan ause_message.HandlerResponseMessage, id string, body models.EapSession) {
	var response models.EapSession

	if !ause_context.CheckIfAuseUeContextExists(id) {
		logger.EapAuthComfirmLog.Infoln("SUPI does not exist, confirmation failed")
		var problemDetails models.ProblemDetails
		problemDetails.Cause = "USER_NOT_FOUND"
		ause_message.SendHttpResponseMessage(respChan, nil, http.StatusBadRequest, problemDetails)
		return
	}
	auseCurrentContext := ause_context.GetAuseUeContext(id)
	servingNetworkName := auseCurrentContext.ServingNetworkName
	eapPayload, _ := base64.StdEncoding.DecodeString(body.EapPayload)
	// fmt.Printf("eapPayload = %x\n", eapPayload)

	eapGoPkt := gopacket.NewPacket(eapPayload, layers.LayerTypeEAP, gopacket.Default)
	eapLayer := eapGoPkt.Layer(layers.LayerTypeEAP)
	eapContent, _ := eapLayer.(*layers.EAP)
	// fmt.Printf("d.Code=%x,\nd.Identitifier=%x,\nd.Type=%x,\nd.Data=%x\n", eapContent.Code, eapContent.Id, eapContent.Type, eapContent.TypeData)

	if eapContent.Code != layers.EAPCodeResponse {
		logConfirmFailureAndInformUDM(id, models.AuthType_EAP_AKA_PRIME, servingNetworkName, "eap packet code error", auseCurrentContext.UdmUeauUrl)
		auseCurrentContext.AuthStatus = models.AuthResult_FAILURE
		response.AuthResult = models.AuthResult_ONGOING
		failEapAkaNoti := ConstructFailEapAkaNotification(eapContent.Id)
		response.EapPayload = failEapAkaNoti
		ause_message.SendHttpResponseMessage(respChan, nil, http.StatusOK, response)
		return
	}

	switch auseCurrentContext.AuthStatus {
	case models.AuthResult_ONGOING:
		response.KSeaf = auseCurrentContext.Kseaf
		response.Supi = id
		Kautn := auseCurrentContext.K_aut
		XRES := auseCurrentContext.XRES
		RES, decodeOK := decodeResMac(eapContent.TypeData, eapContent.Contents, Kautn)
		if !decodeOK {
			auseCurrentContext.AuthStatus = models.AuthResult_FAILURE
			response.AuthResult = models.AuthResult_ONGOING
			logConfirmFailureAndInformUDM(id, models.AuthType_EAP_AKA_PRIME, servingNetworkName, "eap packet decode error", auseCurrentContext.UdmUeauUrl)
			failEapAkaNoti := ConstructFailEapAkaNotification(eapContent.Id)
			response.EapPayload = failEapAkaNoti

		} else if XRES == string(RES) { // decodeOK && XRES == res, auth success
			logger.EapAuthComfirmLog.Infoln("Correct RES value, EAP-AKA' auth succeed")
			response.AuthResult = models.AuthResult_SUCCESS
			eapSuccPkt := ConstructEapNoTypePkt(radius.EapCodeSuccess, eapContent.Id)
			response.EapPayload = eapSuccPkt
			udmUrl := auseCurrentContext.UdmUeauUrl
			if sendErr := sendAuthResultToUDM(id, models.AuthType_EAP_AKA_PRIME, true, servingNetworkName, udmUrl); sendErr != nil {
				logger.EapAuthComfirmLog.Infoln(sendErr.Error())
				var problemDetails models.ProblemDetails
				problemDetails.Cause = "UPSTREAM_SERVER_ERROR"
				ause_message.SendHttpResponseMessage(respChan, nil, http.StatusInternalServerError, problemDetails)
				return
			}
			auseCurrentContext.AuthStatus = models.AuthResult_SUCCESS

		} else { // decodeOK but XRES != res, auth failed
			// fmt.Printf("XRES = %x\nstring(RES) = %x\n", XRES, RES)
			auseCurrentContext.AuthStatus = models.AuthResult_FAILURE
			response.AuthResult = models.AuthResult_ONGOING
			logConfirmFailureAndInformUDM(id, models.AuthType_EAP_AKA_PRIME, servingNetworkName, "Wrong RES value, EAP-AKA' auth failed", auseCurrentContext.UdmUeauUrl)
			failEapAkaNoti := ConstructFailEapAkaNotification(eapContent.Id)
			response.EapPayload = failEapAkaNoti
		}

	case models.AuthResult_FAILURE:
		eapFailPkt := ConstructEapNoTypePkt(radius.EapCodeFailure, uint8(eapPayload[1]))
		response.EapPayload = eapFailPkt
		response.AuthResult = models.AuthResult_FAILURE
	}

	ause_message.SendHttpResponseMessage(respChan, nil, http.StatusOK, response)
}

func HandleAuth5gAkaComfirmRequest(respChan chan ause_message.HandlerResponseMessage, id string, body models.ConfirmationData) {
	var response models.ConfirmationDataResponse
	success := false
	response.AuthResult = models.AuthResult_FAILURE

	if !ause_context.CheckIfAuseUeContextExists(id) {
		logger.Auth5gAkaComfirmLog.Infoln("SUPI does not exist, confirmation failed")
		var problemDetails models.ProblemDetails
		problemDetails.Cause = "USER_NOT_FOUND"
		ause_message.SendHttpResponseMessage(respChan, nil, http.StatusBadRequest, problemDetails)
		return
	}

	auseCurrentContext := ause_context.GetAuseUeContext(id)
	servingNetworkName := auseCurrentContext.ServingNetworkName

	// Compare the received RES* with the stored XRES*
	// fmt.Printf("res*: %x\nXres*: %x\n", body.ResStar, auseCurrentContext.XresStar)
	if strings.Compare(body.ResStar, auseCurrentContext.XresStar) == 0 {
		auseCurrentContext.AuthStatus = models.AuthResult_SUCCESS
		response.AuthResult = models.AuthResult_SUCCESS
		success = true
		logger.Auth5gAkaComfirmLog.Infoln("5G AKA confirmation succeeded")
		response.Kseaf = auseCurrentContext.Kseaf
	} else {
		auseCurrentContext.AuthStatus = models.AuthResult_FAILURE
		response.AuthResult = models.AuthResult_FAILURE
		logConfirmFailureAndInformUDM(id, models.AuthType__5_G_AKA, servingNetworkName, "5G AKA confirmation failed", auseCurrentContext.UdmUeauUrl)
	}

	if sendErr := sendAuthResultToUDM(id, models.AuthType__5_G_AKA, success, servingNetworkName, auseCurrentContext.UdmUeauUrl); sendErr != nil {
		logger.Auth5gAkaComfirmLog.Infoln(sendErr.Error())
		var problemDetails models.ProblemDetails
		problemDetails.Cause = "UPSTREAM_SERVER_ERROR"
		ause_message.SendHttpResponseMessage(respChan, nil, http.StatusInternalServerError, problemDetails)

		return
	}

	response.Supi = id
	ause_message.SendHttpResponseMessage(respChan, nil, http.StatusOK, response)
}

func HandleUeAuthPostRequest(respChan chan ause_message.HandlerResponseMessage, body models.AuthenticationInfo) {
	var response models.UeAuthenticationCtx
	var authInfoReq models.AuthenticationInfoRequest

	supiOrSuci := body.SupiOrSuci
	// fmt.Println("Got supi:", supiOrSuci)

	// check if SEAF is authorized to use the serving network name as in 33501 clause 6.1.2
	snName := body.ServingNetworkName
	servingNetworkAuthorized := ause_context.IsServingNetworkAuthorized(snName)
	if !servingNetworkAuthorized {
		var problemDetails models.ProblemDetails
		problemDetails.Cause = "SERVING_NETWORK_NOT_AUTHORIZED"
		logger.UeAuthPostLog.Infoln("403 forbidden: serving network NOT AUTHORIZED")
		ause_message.SendHttpResponseMessage(respChan, nil, http.StatusForbidden, problemDetails)
		return
	}
	logger.UeAuthPostLog.Infoln("Serving network authorized")

	response.ServingNetworkName = snName
	authInfoReq.ServingNetworkName = snName
	self := ause_context.GetSelf()
	authInfoReq.AuseInstanceId = self.GetSelfID()

	udmUrl := getUdmUrl(self.NrfUri)
	client := createClientToUdmUeau(udmUrl)
	authInfoResult, _, err := client.GenerateAuthDataApi.GenerateAuthData(context.Background(), supiOrSuci, authInfoReq)
	if err != nil {
		logger.UeAuthPostLog.Infoln(err.Error())
		var problemDetails models.ProblemDetails
		if authInfoResult.AuthenticationVector == nil {
			problemDetails.Cause = "AV_GENERATION_PROBLEM"
		} else {
			problemDetails.Cause = "UPSTREAM_SERVER_ERROR"
		}
		ause_message.SendHttpResponseMessage(respChan, nil, http.StatusInternalServerError, problemDetails)
		return
	}

	ueid := authInfoResult.Supi
	auseUeContext := ause_context.NewAuseUeContext(ueid)
	ause_context.AddAuseUeContextToPool(auseUeContext)
	auseUeContext.ServingNetworkName = snName
	auseUeContext.AuthStatus = models.AuthResult_ONGOING
	auseUeContext.UdmUeauUrl = udmUrl

	locationURI := self.Url + "/nause-auth/v1/ue-authentications/" + ueid
	putLink := locationURI
	if authInfoResult.AuthType == models.AuthType__5_G_AKA {
		logger.UeAuthPostLog.Infoln("Use 5G AKA auth method")
		putLink += "/5g-aka-confirmation"

		// Derive HXRES* from XRES*
		concat := authInfoResult.AuthenticationVector.Rand + authInfoResult.AuthenticationVector.XresStar
		hxresStarBytes, _ := hex.DecodeString(concat)
		hxresStarAll := sha256.Sum256(hxresStarBytes)
		hxresStar := hex.EncodeToString(hxresStarAll[16:]) // last 128 bits
		// fmt.Printf("5G AV Rand: %s,  XresStar: %s, Autn: %s\n", authInfoResult.AuthenticationVector.Rand, authInfoResult.AuthenticationVector.XresStar, authInfoResult.AuthenticationVector.Autn)
		// fmt.Printf("hxresStar = %x\n", hxresStar) // 231b3f2e0a8a5082c19fdd6735888c4a

		// Derive Kseaf from Kause
		// Test data
		// P0 := "internet"
		Kause := authInfoResult.AuthenticationVector.Kause
		KauseDecode, _ := hex.DecodeString(Kause)
		P0 := []byte(snName)
		Kseaf := UeauCommon.GetKDFValue(KauseDecode, UeauCommon.FC_FOR_KSEAF_DERIVATION, P0, UeauCommon.KDFLen(P0))
		auseUeContext.XresStar = authInfoResult.AuthenticationVector.XresStar
		auseUeContext.Kause = Kause
		auseUeContext.Kseaf = hex.EncodeToString(Kseaf)

		var av5gAka models.Av5gAka
		av5gAka.Rand = authInfoResult.AuthenticationVector.Rand
		av5gAka.Autn = authInfoResult.AuthenticationVector.Autn
		av5gAka.HxresStar = hxresStar

		response.Var5gAuthData = av5gAka

	} else if authInfoResult.AuthType == models.AuthType_EAP_AKA_PRIME {
		logger.UeAuthPostLog.Infoln("Use EAP-AKA' auth method")
		putLink += "/eap-session"

		identity := ueid
		ikPrime := authInfoResult.AuthenticationVector.IkPrime
		ckPrime := authInfoResult.AuthenticationVector.CkPrime
		RAND := authInfoResult.AuthenticationVector.Rand
		AUTN := authInfoResult.AuthenticationVector.Autn
		XRES := authInfoResult.AuthenticationVector.Xres
		auseUeContext.XRES = XRES

		// Test data
		// identity = "0555444333222111"

		K_encr, K_aut, K_re, MSK, EMSK := eapAkaPrimePrf(ikPrime, ckPrime, identity)
		_, _, _, _, _ = K_encr, K_aut, K_re, MSK, EMSK
		auseUeContext.K_aut = K_aut
		Kause := EMSK[0:32]
		auseUeContext.Kause = Kause
		KauseDecode, _ := hex.DecodeString(Kause)
		P0 := []byte(snName)
		Kseaf := UeauCommon.GetKDFValue(KauseDecode, UeauCommon.FC_FOR_KSEAF_DERIVATION, P0, UeauCommon.KDFLen(P0))
		auseUeContext.Kseaf = hex.EncodeToString(Kseaf)

		var eapPkt radius.EapPacket
		var randIdentifier int
		rand.Seed(time.Now().Unix())

		eapPkt.Code = radius.EapCode(1)
		randIdentifier = rand.Intn(256)
		eapPkt.Identifier = uint8(randIdentifier)
		eapPkt.Type = radius.EapType(50) // accroding to RFC5448 6.1
		atRand, _ := EapEncodeAttribute("AT_RAND", RAND)
		atAutn, _ := EapEncodeAttribute("AT_AUTN", AUTN)
		atKdf, _ := EapEncodeAttribute("AT_KDF", snName)
		atKdfInput, _ := EapEncodeAttribute("AT_KDF_INPUT", snName)
		atMAC, _ := EapEncodeAttribute("AT_MAC", "")

		dataArrayBeforeMAC := atRand + atAutn + atMAC + atKdf + atKdfInput
		eapPkt.Data = []byte(dataArrayBeforeMAC)
		encodedPktBeforeMAC := eapPkt.Encode()

		MACvalue := CalculateAtMAC([]byte(K_aut), encodedPktBeforeMAC)
		// fmt.Printf("MAC value = %x\n", MACvalue)
		atMacNum := fmt.Sprintf("%02x", ause_context.AT_MAC_ATTRIBUTE)
		atMACfirstRow, _ := hex.DecodeString(atMacNum + "05" + "0000")
		wholeAtMAC := append(atMACfirstRow, MACvalue...)

		atMAC = string(wholeAtMAC)
		dataArrayAfterMAC := atRand + atAutn + atMAC + atKdf + atKdfInput

		eapPkt.Data = []byte(dataArrayAfterMAC)
		encodedPktAfterMAC := eapPkt.Encode()
		response.Var5gAuthData = base64.StdEncoding.EncodeToString(encodedPktAfterMAC)

		// fmt.Printf("p.Code=%x,\np.Identitifier=%x,\np.Type=%x,\np.Data=%x\n", byte(eapPkt.Code), byte(eapPkt.Identifier), byte(eapPkt.Type), eapPkt.Data)
		// fmt.Printf("encodedPktAfterMAC: %x\n", encodedPktAfterMAC)
		// fmt.Printf("Var5gAuthData: %x\n", response.Var5gAuthData)
	}

	var linksValue = models.LinksValueSchema{Href: putLink}
	response.Links = make(map[string]models.LinksValueSchema)
	response.Links["link"] = linksValue
	response.AuthType = authInfoResult.AuthType

	respHeader := make(http.Header)
	respHeader.Set("Location", locationURI)
	ause_message.SendHttpResponseMessage(respChan, respHeader, http.StatusCreated, response)
}
