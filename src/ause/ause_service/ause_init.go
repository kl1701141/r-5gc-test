package ause_service

import (
	"bufio"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"free5gc/lib/http2_util"
	"free5gc/lib/path_util"
	"free5gc/src/app"
	"free5gc/src/ause/UEAuthentication"
	"free5gc/src/ause/ause_consumer"
	"free5gc/src/ause/ause_context"
	"free5gc/src/ause/ause_handler"
	"free5gc/src/ause/ause_util"
	"free5gc/src/ause/factory"
	"free5gc/src/ause/logger"
	"os/exec"
	"sync"
)

type AUSE struct{}

type (
	// Config information.
	Config struct {
		ausecfg string
	}
)

var config Config

var auseCLi = []cli.Flag{
	cli.StringFlag{
		Name:  "free5gccfg",
		Usage: "common config file",
	},
	cli.StringFlag{
		Name:  "ausecfg",
		Usage: "config file",
	},
}

var initLog *logrus.Entry

func init() {
	initLog = logger.InitLog
}

func (*AUSE) GetCliCmd() (flags []cli.Flag) {
	return auseCLi
}

func (*AUSE) Initialize(c *cli.Context) {

	config = Config{
		ausecfg: c.String("ausecfg"),
	}

	if config.ausecfg != "" {
		factory.InitConfigFactory(config.ausecfg)
	} else {
		DefaultAuseConfigPath := path_util.Gofree5gcPath("free5gc/config/ausecfg.conf")
		factory.InitConfigFactory(DefaultAuseConfigPath)
	}

	initLog.Traceln("AUSE debug level(string):", app.ContextSelf().Logger.AUSE.DebugLevel)
	if app.ContextSelf().Logger.AUSE.DebugLevel != "" {
		initLog.Infoln("AUSE debug level(string):", app.ContextSelf().Logger.AUSE.DebugLevel)
		level, err := logrus.ParseLevel(app.ContextSelf().Logger.AUSE.DebugLevel)
		if err == nil {
			logger.SetLogLevel(level)
		}
	}

	logger.SetReportCaller(app.ContextSelf().Logger.AUSE.ReportCaller)

}

func (ause *AUSE) FilterCli(c *cli.Context) (args []string) {
	for _, flag := range ause.GetCliCmd() {
		name := flag.GetName()
		value := fmt.Sprint(c.Generic(name))
		if value == "" {
			continue
		}

		args = append(args, "--"+name, value)
	}
	return args
}

func (ause *AUSE) Start() {
	initLog.Infoln("Server started")

	router := gin.Default()
	UEAuthentication.AddService(router)

	ause_context.Init()
	self := ause_context.GetSelf()
	// Register to NRF
	profile, err := ause_consumer.BuildNFInstance(self)
	if err != nil {
		initLog.Error("Build AUSE Profile Error")
	}
	_, self.NfId, err = ause_consumer.SendRegisterNFInstance(self.NrfUri, self.NfId, profile)
	if err != nil {
		initLog.Errorf("AUSE register to NRF Error[%s]", err.Error())
	}

	auseLogPath := ause_util.AuseLogPath
	ausePemPath := ause_util.AusePemPath
	auseKeyPath := ause_util.AuseKeyPath

	go ause_handler.Handle()
	server, err := http2_util.NewServer(":29509", auseLogPath, router)
	if err == nil && server != nil {
		initLog.Infoln(server.ListenAndServeTLS(ausePemPath, auseKeyPath))
	}
}

func (ause *AUSE) Exec(c *cli.Context) error {

	//AUSE.Initialize(cfgPath, c)

	initLog.Traceln("args:", c.String("ausecfg"))
	args := ause.FilterCli(c)
	initLog.Traceln("filter: ", args)
	command := exec.Command("./ause", args...)

	stdout, err := command.StdoutPipe()
	if err != nil {
		initLog.Fatalln(err)
	}
	wg := sync.WaitGroup{}
	wg.Add(3)
	go func() {
		in := bufio.NewScanner(stdout)
		for in.Scan() {
			fmt.Println(in.Text())
		}
		wg.Done()
	}()

	stderr, err := command.StderrPipe()
	if err != nil {
		initLog.Fatalln(err)
	}
	go func() {
		in := bufio.NewScanner(stderr)
		for in.Scan() {
			fmt.Println(in.Text())
		}
		wg.Done()
	}()

	go func() {
		startErr := command.Start()
		if startErr != nil {
			initLog.Fatalln(startErr)
		}
		wg.Done()
	}()

	wg.Wait()

	return err
}
