package ause_context

import (
	// "fmt"
	"free5gc/lib/openapi/models"
	"regexp"
)

type AUSEContext struct {
	NfId            string
	GroupId         string
	HttpIpv4Port    int
	HttpIPv4Address string
	Url             string
	UriScheme       models.UriScheme
	NrfUri          string
	NfService       map[models.ServiceName]models.NfService
	PlmnList        []models.PlmnId
	UdmUeauUrl      string
}

type AuseUeContext struct {
	Supi               string
	Kause              string
	Kseaf              string
	ServingNetworkName string
	AuthStatus         models.AuthResult
	UdmUeauUrl         string

	// for 5G AKA
	XresStar string

	// for EAP-AKA'
	K_aut string
	XRES  string
}

const (
	EAP_AKA_PRIME_TYPENUM = 50
)

// Attribute Types for EAP-AKA'
const (
	AT_RAND_ATTRIBUTE         = 1
	AT_AUTN_ATTRIBUTE         = 2
	AT_RES_ATTRIBUTE          = 3
	AT_MAC_ATTRIBUTE          = 11
	AT_NOTIFICATION_ATTRIBUTE = 12
	AT_IDENTITY_ATTRIBUTE     = 14
	AT_KDF_INPUT_ATTRIBUTE    = 23
	AT_KDF_ATTRIBUTE          = 24
)

var auseContext AUSEContext
var auseUeContextPool map[string]*AuseUeContext
var snRegex *regexp.Regexp

func Init() {
	auseUeContextPool = make(map[string]*AuseUeContext)
	snRegex, _ = regexp.Compile("5G:mnc[0-9]{3}[.]mcc[0-9]{3}[.]3gppnetwork[.]org")
	InitAuseContext(&auseContext)
}

func NewAuseUeContext(identifier string) (auseUeContext *AuseUeContext) {
	auseUeContext = new(AuseUeContext)
	auseUeContext.Supi = identifier // supi
	return auseUeContext
}

func AddAuseUeContextToPool(auseUeContext *AuseUeContext) {
	auseUeContextPool[auseUeContext.Supi] = auseUeContext
}

func CheckIfAuseUeContextExists(ref string) bool {
	return (auseUeContextPool[ref] != nil)
}

func GetAuseUeContext(ref string) (auseUeContext *AuseUeContext) {
	auseUeContext = auseUeContextPool[ref]
	return auseUeContext
}

func IsServingNetworkAuthorized(lookup string) bool {
	if snRegex.MatchString(lookup) {
		return true
	} else {
		return false
	}
}

func GetSelf() *AUSEContext {
	return &auseContext
}

func (a AUSEContext) GetSelfID() string {
	return a.NfId
}
