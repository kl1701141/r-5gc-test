package ause_context

import (
	"fmt"
	"github.com/google/uuid"
	"free5gc/lib/openapi/models"
	"free5gc/lib/path_util"
	"free5gc/src/ause/factory"
	"free5gc/src/ause/logger"
	"strconv"
)

func TestInit() {
	// load config
	DefaultAuseConfigPath := path_util.Gofree5gcPath("free5gc/config/ausecfg.conf")
	factory.InitConfigFactory(DefaultAuseConfigPath)
	Init()
}

func InitAuseContext(context *AUSEContext) {
	config := factory.AuseConfig
	logger.InitLog.Infof("auseconfig Info: Version[%s] Description[%s]\n", config.Info.Version, config.Info.Description)

	configuration := config.Configuration
	sbi := configuration.Sbi

	context.NfId = uuid.New().String()
	context.GroupId = configuration.GroupId
	context.NrfUri = configuration.NrfUri
	context.UriScheme = models.UriScheme_HTTPS // default uri scheme
	context.HttpIPv4Address = "127.0.0.1"      // default localhost
	context.HttpIpv4Port = 29509               // default port
	if sbi != nil {
		if sbi.IPv4Addr != "" {
			context.HttpIPv4Address = sbi.IPv4Addr
		}
		if sbi.Port != 0 {
			context.HttpIpv4Port = sbi.Port
		}

		if sbi.Scheme == "https" {
			context.UriScheme = models.UriScheme_HTTPS
		} else {
			context.UriScheme = models.UriScheme_HTTP
		}
	}

	context.Url = string(context.UriScheme) + "://" + context.HttpIPv4Address + ":" + strconv.Itoa(context.HttpIpv4Port)
	context.PlmnList = append(context.PlmnList, configuration.PlmnSupportList...)

	// context.NfService
	context.NfService = make(map[models.ServiceName]models.NfService)
	AddNfServices(&context.NfService, &config, context)
	fmt.Println("ause context = ", context)
}

func AddNfServices(serviceMap *map[models.ServiceName]models.NfService, config *factory.Config, context *AUSEContext) {
	var nfService models.NfService
	var ipEndPoints []models.IpEndPoint
	var nfServiceVersions []models.NfServiceVersion
	services := *serviceMap

	// nause-auth
	nfService.ServiceInstanceId = context.NfId
	nfService.ServiceName = models.ServiceName_NAUSE_AUTH

	var ipEndPoint models.IpEndPoint
	ipEndPoint.Ipv4Address = context.HttpIPv4Address
	ipEndPoint.Port = int32(context.HttpIpv4Port)
	ipEndPoints = append(ipEndPoints, ipEndPoint)

	var nfServiceVersion models.NfServiceVersion
	nfServiceVersion.ApiFullVersion = config.Info.Version
	nfServiceVersion.ApiVersionInUri = "v1"
	nfServiceVersions = append(nfServiceVersions, nfServiceVersion)

	nfService.Scheme = context.UriScheme
	nfService.NfServiceStatus = models.NfServiceStatus_REGISTERED

	nfService.IpEndPoints = &ipEndPoints
	nfService.Versions = &nfServiceVersions
	services[models.ServiceName_NAUSE_AUTH] = nfService
}
